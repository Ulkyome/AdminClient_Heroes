﻿namespace AdminClient
{
    using AdminClientServiceCore.Messages;
    using Devcat.Core;
    using Devcat.Core.Net;
    using Devcat.Core.Net.Message;
    using global::AdminClient.Properties;
    using System;

    public class AdminClientNode
    {
        private AdminClientForm parent;
        private TcpClient peer = new TcpClient();
        private bool valid;

        public event EventHandler<EventArgs> ConnectionFailed;

        public event EventHandler<EventArgs> ConnectionSucceed;

        public event EventHandler<EventArgs> Disconnected;

        public AdminClientNode(AdminClientForm p)
        {
            this.parent = p;
            this.valid = false;
            this.peer.Connect(p.Thread, Settings.Default.ConnectIP, Settings.Default.ConnectPort, new MessageAnalyzer());
            this.peer.ConnectionSucceed += new EventHandler<EventArgs>(this.AtConnectionSucceed);
            this.peer.ConnectionFail += new EventHandler<EventArgs<Exception>>(this.AtConnectionFailed);
            this.peer.Disconnected += new EventHandler<EventArgs>(this.OnDisconnected);
            this.peer.PacketReceive += new EventHandler<EventArgs<ArraySegment<byte>>>(this.OnPacketReceive);
        }

        private void AtConnectionFailed(object sender, EventArgs e)
        {
            this.valid = false;
            this.ConnectionFailed(sender, e);
        }

        private void AtConnectionSucceed(object sender, EventArgs e)
        {
            this.valid = true;
            this.ConnectionSucceed(sender, e);
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            this.Disconnected(sender, e);
        }

        private void OnPacketReceive(object sender, EventArgs<ArraySegment<byte>> e)
        {
            Packet packet = new Packet(e.Value);
            this.parent.MF.Handle(packet, this);
        }

        public void ProcessMessage(object message)
        {
            if (message is AdminReportClientcountMessage)
            {
                AdminReportClientcountMessage m = message as AdminReportClientcountMessage;
                this.parent.Invoke(() => this.parent.UpdateUserCountText(m.Value, m.Total, m.Waiting, m.States));
            }
            if (message is AdminReportEmergencyStopMessage)
            {
                AdminReportEmergencyStopMessage m = message as AdminReportEmergencyStopMessage;
                this.parent.Invoke(() => this.parent.UpdateStoppedServices(m.ServiceList));
            }
        }

        public void RequestConsoleCommand(string text, bool isServerCommand)
        {
            AdminBroadcastConsoleCommandMessage message = new AdminBroadcastConsoleCommandMessage {
                commandString = text,
                isServerCommand = isServerCommand
            };
            this.peer.Transmit(SerializeWriter.ToBinary<AdminBroadcastConsoleCommandMessage>(message));
        }

        public void RequestItemFestival(string ItemClass, int Amount, string Message, bool IsCafe)
        {
            AdminItemFestivalEventMessage message = new AdminItemFestivalEventMessage {
                Amount = Amount,
                ItemClass = ItemClass,
                Message = Message,
                IsCafe = IsCafe
            };
            this.peer.Transmit(SerializeWriter.ToBinary<AdminItemFestivalEventMessage>(message));
        }

        public void RequestKick(string uid, string cid)
        {
            if (cid == "")
            {
                this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestKickMessage>(new AdminRequestKickMessage(uid, true)));
            }
            else
            {
                this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestKickMessage>(new AdminRequestKickMessage(cid, false)));
            }
        }

        public void RequestNotify(string text)
        {
            this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestNotifyMessage>(new AdminRequestNotifyMessage(text)));
        }

        public void RequestServerCommand(string text, string arg)
        {
            this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestConsoleCommandMessage>(new AdminRequestConsoleCommandMessage(text, arg)));
        }

        public void RequestShutDown(int time, string announce)
        {
            this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestShutDownMessage>(new AdminRequestShutDownMessage(time, announce)));
        }

        public void RequestStopService(string target, bool state)
        {
            AdminRequestEmergencyStopMessage message = new AdminRequestEmergencyStopMessage {
                TargetService = target,
                TargetState = state
            };
            this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestEmergencyStopMessage>(message));
        }

        public void RequestUserCount()
        {
            this.peer.Transmit(SerializeWriter.ToBinary<AdminRequestClientCountMessage>(new AdminRequestClientCountMessage()));
        }

        public bool Valid
        {
            get
            {
                return this.valid;
            }
        }
    }
}

