﻿namespace AdminClient
{
    using global::AdminClient.Properties;
    using System;
    using System.Data;
    using System.Reflection;

    [Database(Name="heroes")]
    public class GMAccountDBDataContext : DataContext
    {
        private static MappingSource mappingSource = new AttributeMappingSource();

        public GMAccountDBDataContext() : base(Settings.Default.heroesConnectionString, mappingSource)
        {
            this.OnCreated();
        }

        public GMAccountDBDataContext(IDbConnection connection) : base(connection, mappingSource)
        {
            this.OnCreated();
        }

        public GMAccountDBDataContext(string connection) : base(connection, mappingSource)
        {
            this.OnCreated();
        }

        public GMAccountDBDataContext(IDbConnection connection, MappingSource mappingSource) : base(connection, mappingSource)
        {
            this.OnCreated();
        }

        public GMAccountDBDataContext(string connection, MappingSource mappingSource) : base(connection, mappingSource)
        {
            this.OnCreated();
        }

        [Function(Name="dbo.ExtendCashItems")]
        public int ExtendCashItems([Parameter(DbType="DateTime2")] DateTime? fromDate, [Parameter(DbType="Int")] int? minutes)
        {
            return (int) base.ExecuteMethodCall(this, (MethodInfo) MethodBase.GetCurrentMethod(), new object[] { fromDate, minutes }).ReturnValue;
        }

        private void OnCreated()
        {
            base.CommandTimeout = 0xe10;
        }

        public Table<AdminClient.GMAccounts> GMAccounts
        {
            get
            {
                return base.GetTable<AdminClient.GMAccounts>();
            }
        }
    }
}

