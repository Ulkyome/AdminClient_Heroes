﻿namespace AdminClient
{
    using AdminClientServiceCore.Messages;
    using Devcat.Core.Net.Message;
    using Devcat.Core.Threading;
    using global::AdminClient.Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Security.Principal;
    using System.Text;
    using System.Windows.Forms;

    public class AdminClientForm : Form
    {
        private AdminClientNode adminNode;
        private CheckBox CafeCheckBox;
        private NumericUpDown CashExtendMinutesUpDown1;
        private Label CharacterIDLabel;
        private Button ClientCommandButton;
        private IContainer components;
        private TextBox ConsoleCommandTextBox;
        private Button CopyToClipBoardButton;
        private Button ExpandButton_a;
        private Button ExpandButton_b;
        private Button ExpandButton_c;
        private Button ExtendExpireTimeButton;
        private int formHeight = 190;
        private Button GameServerCommandButton;
        private Button HostCommandButton;
        private NumericUpDown ItemFestivalAmountUpDown;
        private Button ItemFestivalButton;
        private TextBox ItemFestivalClassNameTextBox;
        private TextBox ItemFestivalMessageTextBox;
        private TextBox KickCharacterIDTextBox;
        private Button KickUserButton;
        private TextBox KickUserIDTextBox;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private int LatestUserCount;
        private MessageHandlerFactory mf = new MessageHandlerFactory();
        private Button NotifyButton;
        private TextBox NotifyTextBox;
        private Button RefreshUserCountButton;
        private Button ResumeServiceButton;
        private ComboBox ResumeServiceSelectComboBox;
        private Button ShurinkButton_a;
        private Button ShurinkButton_b;
        private Button ShurinkButton_c;
        private Button ShutDownBottuon;
        private ListBox ShutDownTimeListBox;
        private Button StopServiceButton;
        private ComboBox StopServiceSelectComboBox;
        private JobProcessor thread = new JobProcessor();
        private TextBox UserCountTextBox;
        private Label UserIDLabel;

        public AdminClientForm()
        {
            this.InitializeComponent();
            this.mf.Register<AdminClientNode>(AdminClientServiceOperationMessages.TypeConverters, "ProcessMessage");
            this.thread.Start();
        }

        private void AdminClientForm_Load(object sender, EventArgs e)
        {
            this.adminNode = new AdminClientNode(this);
            this.adminNode.ConnectionSucceed += new EventHandler<EventArgs>(this.OnConnected);
            this.adminNode.ConnectionFailed += new EventHandler<EventArgs>(this.OnFailed);
            this.adminNode.Disconnected += new EventHandler<EventArgs>(this.OnDisconnected);
            base.Height = this.formHeight = 190;
            this.NotifyButton.Visible = this.NotifyTextBox.Visible = false;
            this.ClientCommandButton.Visible = this.ConsoleCommandTextBox.Visible = false;
            this.ShurinkButton_a.Visible = this.ExpandButton_a.Visible = true;
            this.ShurinkButton_b.Visible = this.ExpandButton_b.Visible = false;
            this.ShurinkButton_c.Visible = this.ExpandButton_c.Visible = false;
            if (Settings.Default.ConnectPort == 0x6983)
            {
                this.Text = this.Text + " : Main Server";
            }
            else if (Settings.Default.ConnectPort == 0x6982)
            {
                this.Text = this.Text + " : Test Server";
            }
            else if (Settings.Default.ConnectPort == 0x6981)
            {
                this.Text = this.Text + " : Staging Server";
            }
        }

        private void AdminClientForm_Resize(object sender, EventArgs e)
        {
            base.Height = this.formHeight;
        }

        private void ClientCommandButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(this.ConsoleCommandTextBox.Text);
            builder.AppendLine();
            builder.AppendLine("---");
            builder.AppendLine();
            builder.AppendLine("중간의 빈 줄은 자동적으로 무시됩니다.");
            builder.AppendLine("위 명령을 실행할까요?");
            if (MessageBox.Show(builder.ToString(), "클라이언트 커맨드 실행", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                this.adminNode.RequestConsoleCommand(this.ConsoleCommandTextBox.Text, false);
            }
            base.Enabled = true;
        }

        private void CopyToClipBoardButton_Click(object sender, EventArgs e)
        {
            char[] separator = new char[] { '@' };
            string[] strArray = this.UserCountTextBox.Text.Replace("\r\n", "@").Split(separator);
            if (strArray.Length > 2)
            {
                Clipboard.SetText(strArray[strArray.Length - 2]);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ExpandButton_a_Click(object sender, EventArgs e)
        {
            try
            {
                using (GMAccountDBDataContext context = new GMAccountDBDataContext())
                {
                    if ((from line in context.GMAccounts
                        where line.ID == WindowsIdentity.GetCurrent().Name.ToString()
                        select line).Count<GMAccounts>() == 0)
                    {
                        MessageBox.Show("접근 권한이 없습니다!", "에러", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                        base.Height = this.formHeight = 370;
                        this.NotifyButton.Visible = this.NotifyTextBox.Visible = true;
                        this.ClientCommandButton.Visible = this.ConsoleCommandTextBox.Visible = false;
                        this.ShurinkButton_a.Visible = this.ExpandButton_a.Visible = false;
                        this.ShurinkButton_b.Visible = this.ExpandButton_b.Visible = true;
                        this.ShurinkButton_c.Visible = this.ExpandButton_c.Visible = false;
                    }
                }
            }
            catch
            {
                MessageBox.Show("DB 접근에 실패했습니다!", "에러", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ExpandButton_b_Click(object sender, EventArgs e)
        {
            try
            {
                using (GMAccountDBDataContext context = new GMAccountDBDataContext())
                {
                    if ((from line in context.GMAccounts
                        where line.ID == WindowsIdentity.GetCurrent().Name.ToString()
                        select line).Count<GMAccounts>() == 0)
                    {
                        MessageBox.Show("접근 권한이 없습니다!", "에러", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                        base.Height = this.formHeight = 0x239;
                        this.NotifyButton.Visible = this.NotifyTextBox.Visible = true;
                        this.ClientCommandButton.Visible = this.ConsoleCommandTextBox.Visible = true;
                        this.ShurinkButton_a.Visible = this.ExpandButton_a.Visible = false;
                        this.ShurinkButton_b.Visible = this.ExpandButton_b.Visible = false;
                        this.ShurinkButton_c.Visible = this.ExpandButton_c.Visible = true;
                    }
                }
            }
            catch
            {
                MessageBox.Show("DB 접근에 실패했습니다!", "에러", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ExtendExpireTimeButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format("{0} 부터 {1} 분 연장", DateTime.Now, this.CashExtendMinutesUpDown1.Value));
            builder.AppendLine("---");
            builder.AppendLine("연장 작업은 대단히 오래 걸릴 수 있습니다. 중간에 프로그램을 끄지 마세요.");
            builder.AppendLine("캐시 아이템 연장 명령을 실행할까요?");
            if ((MessageBox.Show(builder.ToString(), "캐시 아이템 연장", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK) && (this.CashExtendMinutesUpDown1.Value > 0M))
            {
                using (GMAccountDBDataContext context = new GMAccountDBDataContext())
                {
                    context.ExtendCashItems(new DateTime?(DateTime.UtcNow), new int?((int) this.CashExtendMinutesUpDown1.Value));
                }
            }
            base.Enabled = true;
        }

        private void GameServerCommandButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            string str = this.ConsoleCommandTextBox.Text.Split(new char[] { ' ' }).FirstOrDefault<string>();
            if (str != null)
            {
                string str2 = this.ConsoleCommandTextBox.Text.Substring(str.Length).Trim();
                StringBuilder builder = new StringBuilder();
                builder.AppendLine(str);
                builder.AppendLine(str2);
                builder.AppendLine("---");
                builder.AppendLine();
                builder.AppendLine("위 명령을 실행할까요?");
                if (MessageBox.Show(builder.ToString(), "서버 커맨드 실행", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    this.adminNode.RequestServerCommand(str, str2);
                }
            }
            base.Enabled = true;
        }

        private void HostCommandButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(this.ConsoleCommandTextBox.Text);
            builder.AppendLine();
            builder.AppendLine("---");
            builder.AppendLine();
            builder.AppendLine("중간의 빈 줄은 자동적으로 무시됩니다.");
            builder.AppendLine("위 명령을 실행할까요?");
            if (MessageBox.Show(builder.ToString(), "호스트 커맨드 실행", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                this.adminNode.RequestConsoleCommand(this.ConsoleCommandTextBox.Text, true);
            }
            base.Enabled = true;
        }

        private void InitializeComponent()
        {
            this.ShutDownBottuon = new Button();
            this.KickCharacterIDTextBox = new TextBox();
            this.ShutDownTimeListBox = new ListBox();
            this.RefreshUserCountButton = new Button();
            this.NotifyButton = new Button();
            this.KickUserButton = new Button();
            this.NotifyTextBox = new TextBox();
            this.UserCountTextBox = new TextBox();
            this.UserIDLabel = new Label();
            this.CharacterIDLabel = new Label();
            this.StopServiceButton = new Button();
            this.ResumeServiceButton = new Button();
            this.StopServiceSelectComboBox = new ComboBox();
            this.ResumeServiceSelectComboBox = new ComboBox();
            this.KickUserIDTextBox = new TextBox();
            this.ConsoleCommandTextBox = new TextBox();
            this.HostCommandButton = new Button();
            this.ClientCommandButton = new Button();
            this.ShurinkButton_c = new Button();
            this.ExpandButton_a = new Button();
            this.CopyToClipBoardButton = new Button();
            this.ExpandButton_b = new Button();
            this.ShurinkButton_b = new Button();
            this.ExpandButton_c = new Button();
            this.ShurinkButton_a = new Button();
            this.ItemFestivalButton = new Button();
            this.ItemFestivalClassNameTextBox = new TextBox();
            this.ItemFestivalAmountUpDown = new NumericUpDown();
            this.ItemFestivalMessageTextBox = new TextBox();
            this.label1 = new Label();
            this.label2 = new Label();
            this.CafeCheckBox = new CheckBox();
            this.ExtendExpireTimeButton = new Button();
            this.CashExtendMinutesUpDown1 = new NumericUpDown();
            this.label3 = new Label();
            this.label4 = new Label();
            this.GameServerCommandButton = new Button();
            this.ItemFestivalAmountUpDown.BeginInit();
            this.CashExtendMinutesUpDown1.BeginInit();
            base.SuspendLayout();
            this.ShutDownBottuon.Location = new Point(12, 0xbb);
            this.ShutDownBottuon.Name = "ShutDownBottuon";
            this.ShutDownBottuon.Size = new Size(0x75, 0x34);
            this.ShutDownBottuon.TabIndex = 2;
            this.ShutDownBottuon.Text = "서버 종료";
            this.ShutDownBottuon.UseVisualStyleBackColor = true;
            this.ShutDownBottuon.Click += new EventHandler(this.ShutDownBottuon_Click);
            this.KickCharacterIDTextBox.Location = new Point(0xc4, 0x114);
            this.KickCharacterIDTextBox.Name = "KickCharacterIDTextBox";
            this.KickCharacterIDTextBox.Size = new Size(0xfe, 0x15);
            this.KickCharacterIDTextBox.TabIndex = 5;
            this.KickCharacterIDTextBox.TextChanged += new EventHandler(this.KickCharacterIDTextBox_TextChanged);
            this.KickCharacterIDTextBox.Enter += new EventHandler(this.KickCharacterIDTextBox_Enter);
            this.ShutDownTimeListBox.FormattingEnabled = true;
            this.ShutDownTimeListBox.ItemHeight = 12;
            this.ShutDownTimeListBox.Items.AddRange(new object[] { "3600 초 후 종료 (1시간)", "1800 초 후 종료 (30분)", "600 초 후 종료 (10분)", "300 초 후 종료 (5분)", "60 초 후 종료", "15 초 후 종료", "10 초 후 종료", "5 초 후 종료" });
            this.ShutDownTimeListBox.Location = new Point(0x87, 0xbb);
            this.ShutDownTimeListBox.Name = "ShutDownTimeListBox";
            this.ShutDownTimeListBox.Size = new Size(0x13b, 0x34);
            this.ShutDownTimeListBox.TabIndex = 7;
            this.RefreshUserCountButton.Location = new Point(12, 12);
            this.RefreshUserCountButton.Name = "RefreshUserCountButton";
            this.RefreshUserCountButton.Size = new Size(0x75, 0x18);
            this.RefreshUserCountButton.TabIndex = 8;
            this.RefreshUserCountButton.Text = "동접자 확인";
            this.RefreshUserCountButton.UseVisualStyleBackColor = true;
            this.RefreshUserCountButton.Click += new EventHandler(this.RefreshUserCountButton_Click);
            this.NotifyButton.Location = new Point(12, 0x81);
            this.NotifyButton.Name = "NotifyButton";
            this.NotifyButton.Size = new Size(0x75, 0x34);
            this.NotifyButton.TabIndex = 9;
            this.NotifyButton.Text = "공지";
            this.NotifyButton.UseVisualStyleBackColor = true;
            this.NotifyButton.Click += new EventHandler(this.NotifyButton_Click);
            this.KickUserButton.Location = new Point(12, 0xf5);
            this.KickUserButton.Name = "KickUserButton";
            this.KickUserButton.Size = new Size(0x75, 0x34);
            this.KickUserButton.TabIndex = 10;
            this.KickUserButton.Text = "유저 Kick";
            this.KickUserButton.UseVisualStyleBackColor = true;
            this.KickUserButton.Click += new EventHandler(this.KickUserButton_Click);
            this.NotifyTextBox.Location = new Point(0x87, 0x81);
            this.NotifyTextBox.Multiline = true;
            this.NotifyTextBox.Name = "NotifyTextBox";
            this.NotifyTextBox.ScrollBars = ScrollBars.Horizontal;
            this.NotifyTextBox.Size = new Size(0x13b, 0x34);
            this.NotifyTextBox.TabIndex = 11;
            this.NotifyTextBox.Enter += new EventHandler(this.NotifyTextBox_Enter);
            this.UserCountTextBox.BackColor = SystemColors.Window;
            this.UserCountTextBox.Location = new Point(0x88, 12);
            this.UserCountTextBox.Multiline = true;
            this.UserCountTextBox.Name = "UserCountTextBox";
            this.UserCountTextBox.ReadOnly = true;
            this.UserCountTextBox.ScrollBars = ScrollBars.Vertical;
            this.UserCountTextBox.Size = new Size(0x13a, 110);
            this.UserCountTextBox.TabIndex = 12;
            this.UserCountTextBox.WordWrap = false;
            this.UserIDLabel.AutoSize = true;
            this.UserIDLabel.Location = new Point(0x87, 250);
            this.UserIDLabel.Name = "UserIDLabel";
            this.UserIDLabel.Size = new Size(0x2e, 12);
            this.UserIDLabel.TabIndex = 13;
            this.UserIDLabel.Text = "User ID";
            this.CharacterIDLabel.AutoSize = true;
            this.CharacterIDLabel.Location = new Point(0x87, 280);
            this.CharacterIDLabel.Name = "CharacterIDLabel";
            this.CharacterIDLabel.Size = new Size(0x38, 12);
            this.CharacterIDLabel.TabIndex = 14;
            this.CharacterIDLabel.Text = "캐릭터 ID";
            this.StopServiceButton.Location = new Point(12, 0x19d);
            this.StopServiceButton.Name = "StopServiceButton";
            this.StopServiceButton.Size = new Size(0x75, 0x16);
            this.StopServiceButton.TabIndex = 15;
            this.StopServiceButton.Text = "서비스 정지";
            this.StopServiceButton.UseVisualStyleBackColor = true;
            this.StopServiceButton.Click += new EventHandler(this.StopServiceButton_Click);
            this.ResumeServiceButton.Location = new Point(12, 0x1b9);
            this.ResumeServiceButton.Name = "ResumeServiceButton";
            this.ResumeServiceButton.Size = new Size(0x75, 0x16);
            this.ResumeServiceButton.TabIndex = 0x12;
            this.ResumeServiceButton.Text = "서비스 재개";
            this.ResumeServiceButton.UseVisualStyleBackColor = true;
            this.ResumeServiceButton.Click += new EventHandler(this.ResumeServiceButton_Click);
            this.StopServiceSelectComboBox.FormattingEnabled = true;
            this.StopServiceSelectComboBox.Items.AddRange(new object[] { "CashShop (캐시샵)", "CraftItem (아이템 구매/생산)", "CreateCharacter (캐릭터 생성)", "Mailing (우편) [준비중]", "Shipping (배 출발)", "StoryTelling (스토리라인 서비스) [준비중]" });
            this.StopServiceSelectComboBox.Location = new Point(0x89, 0x19f);
            this.StopServiceSelectComboBox.Name = "StopServiceSelectComboBox";
            this.StopServiceSelectComboBox.Size = new Size(0x139, 20);
            this.StopServiceSelectComboBox.Sorted = true;
            this.StopServiceSelectComboBox.TabIndex = 0x13;
            this.ResumeServiceSelectComboBox.FormattingEnabled = true;
            this.ResumeServiceSelectComboBox.Location = new Point(0x89, 0x1b9);
            this.ResumeServiceSelectComboBox.Name = "ResumeServiceSelectComboBox";
            this.ResumeServiceSelectComboBox.Size = new Size(0x139, 20);
            this.ResumeServiceSelectComboBox.Sorted = true;
            this.ResumeServiceSelectComboBox.TabIndex = 20;
            this.KickUserIDTextBox.Location = new Point(0xc4, 0xf5);
            this.KickUserIDTextBox.Name = "KickUserIDTextBox";
            this.KickUserIDTextBox.Size = new Size(0xfe, 0x15);
            this.KickUserIDTextBox.TabIndex = 4;
            this.KickUserIDTextBox.TextChanged += new EventHandler(this.KickUserIDTextBox_TextChanged);
            this.KickUserIDTextBox.Enter += new EventHandler(this.KickUserIDTextBox_Enter);
            this.ConsoleCommandTextBox.Location = new Point(0x87, 0x14d);
            this.ConsoleCommandTextBox.Multiline = true;
            this.ConsoleCommandTextBox.Name = "ConsoleCommandTextBox";
            this.ConsoleCommandTextBox.ScrollBars = ScrollBars.Horizontal;
            this.ConsoleCommandTextBox.Size = new Size(0x13b, 0x48);
            this.ConsoleCommandTextBox.TabIndex = 0x16;
            this.HostCommandButton.Location = new Point(12, 0x163);
            this.HostCommandButton.Name = "HostCommandButton";
            this.HostCommandButton.Size = new Size(0x75, 0x16);
            this.HostCommandButton.TabIndex = 0x17;
            this.HostCommandButton.Text = "호스트 커맨드";
            this.HostCommandButton.UseVisualStyleBackColor = true;
            this.HostCommandButton.Click += new EventHandler(this.HostCommandButton_Click);
            this.ClientCommandButton.Location = new Point(12, 0x14d);
            this.ClientCommandButton.Name = "ClientCommandButton";
            this.ClientCommandButton.Size = new Size(0x75, 0x16);
            this.ClientCommandButton.TabIndex = 0x18;
            this.ClientCommandButton.Text = "클라이언트 커맨드";
            this.ClientCommandButton.UseVisualStyleBackColor = true;
            this.ClientCommandButton.Click += new EventHandler(this.ClientCommandButton_Click);
            this.ShurinkButton_c.Location = new Point(210, 0x227);
            this.ShurinkButton_c.Name = "ShurinkButton_c";
            this.ShurinkButton_c.Size = new Size(0x75, 0x16);
            this.ShurinkButton_c.TabIndex = 0x19;
            this.ShurinkButton_c.Text = "접기";
            this.ShurinkButton_c.UseVisualStyleBackColor = true;
            this.ShurinkButton_c.Click += new EventHandler(this.ShurinkButton_c_Click);
            this.ExpandButton_a.Location = new Point(0x14d, 0x81);
            this.ExpandButton_a.Name = "ExpandButton_a";
            this.ExpandButton_a.Size = new Size(0x75, 0x16);
            this.ExpandButton_a.TabIndex = 0x1a;
            this.ExpandButton_a.Text = "펼치기";
            this.ExpandButton_a.UseVisualStyleBackColor = true;
            this.ExpandButton_a.Click += new EventHandler(this.ExpandButton_a_Click);
            this.CopyToClipBoardButton.Location = new Point(12, 0x2a);
            this.CopyToClipBoardButton.Name = "CopyToClipBoardButton";
            this.CopyToClipBoardButton.Size = new Size(0x75, 0x16);
            this.CopyToClipBoardButton.TabIndex = 0x1b;
            this.CopyToClipBoardButton.Text = "클립보드로 복사";
            this.CopyToClipBoardButton.UseVisualStyleBackColor = true;
            this.CopyToClipBoardButton.Click += new EventHandler(this.CopyToClipBoardButton_Click);
            this.ExpandButton_b.Location = new Point(0x14d, 0x131);
            this.ExpandButton_b.Name = "ExpandButton_b";
            this.ExpandButton_b.Size = new Size(0x75, 0x16);
            this.ExpandButton_b.TabIndex = 0x1c;
            this.ExpandButton_b.Text = "펼치기";
            this.ExpandButton_b.UseVisualStyleBackColor = true;
            this.ExpandButton_b.Click += new EventHandler(this.ExpandButton_b_Click);
            this.ShurinkButton_b.Location = new Point(210, 0x131);
            this.ShurinkButton_b.Name = "ShurinkButton_b";
            this.ShurinkButton_b.Size = new Size(0x75, 0x16);
            this.ShurinkButton_b.TabIndex = 0x1d;
            this.ShurinkButton_b.Text = "접기";
            this.ShurinkButton_b.UseVisualStyleBackColor = true;
            this.ShurinkButton_b.Click += new EventHandler(this.ShurinkButton_b_Click);
            this.ExpandButton_c.Enabled = false;
            this.ExpandButton_c.Location = new Point(0x14d, 0x227);
            this.ExpandButton_c.Name = "ExpandButton_c";
            this.ExpandButton_c.Size = new Size(0x75, 0x16);
            this.ExpandButton_c.TabIndex = 30;
            this.ExpandButton_c.Text = "펼치기";
            this.ExpandButton_c.UseVisualStyleBackColor = true;
            this.ShurinkButton_a.Enabled = false;
            this.ShurinkButton_a.Location = new Point(210, 0x81);
            this.ShurinkButton_a.Name = "ShurinkButton_a";
            this.ShurinkButton_a.Size = new Size(0x75, 0x16);
            this.ShurinkButton_a.TabIndex = 0x1f;
            this.ShurinkButton_a.Text = "접기";
            this.ShurinkButton_a.UseVisualStyleBackColor = true;
            this.ItemFestivalButton.Location = new Point(12, 0x1d5);
            this.ItemFestivalButton.Name = "ItemFestivalButton";
            this.ItemFestivalButton.Size = new Size(0x75, 0x30);
            this.ItemFestivalButton.TabIndex = 0x20;
            this.ItemFestivalButton.Text = "아이템 지급 이벤트";
            this.ItemFestivalButton.UseVisualStyleBackColor = true;
            this.ItemFestivalButton.Click += new EventHandler(this.ItemFestivalButton_Click);
            this.ItemFestivalClassNameTextBox.Location = new Point(0xb2, 0x1d5);
            this.ItemFestivalClassNameTextBox.Name = "ItemFestivalClassNameTextBox";
            this.ItemFestivalClassNameTextBox.Size = new Size(0x9c, 0x15);
            this.ItemFestivalClassNameTextBox.TabIndex = 0x21;
            this.ItemFestivalAmountUpDown.Location = new Point(340, 0x1d5);
            int[] bits = new int[4];
            bits[0] = 0x540be400;
            bits[1] = 2;
            this.ItemFestivalAmountUpDown.Maximum = new decimal(bits);
            int[] numArray2 = new int[4];
            numArray2[0] = 1;
            this.ItemFestivalAmountUpDown.Minimum = new decimal(numArray2);
            this.ItemFestivalAmountUpDown.Name = "ItemFestivalAmountUpDown";
            this.ItemFestivalAmountUpDown.Size = new Size(0x36, 0x15);
            this.ItemFestivalAmountUpDown.TabIndex = 0x22;
            this.ItemFestivalAmountUpDown.TextAlign = HorizontalAlignment.Right;
            int[] numArray3 = new int[4];
            numArray3[0] = 1;
            this.ItemFestivalAmountUpDown.Value = new decimal(numArray3);
            this.ItemFestivalMessageTextBox.Location = new Point(0xb2, 0x1f0);
            this.ItemFestivalMessageTextBox.Name = "ItemFestivalMessageTextBox";
            this.ItemFestivalMessageTextBox.Size = new Size(0x110, 0x15);
            this.ItemFestivalMessageTextBox.TabIndex = 0x23;
            this.label1.AutoSize = true;
            this.label1.Location = new Point(0x87, 0x1f3);
            this.label1.Name = "label1";
            this.label1.Size = new Size(0x29, 12);
            this.label1.TabIndex = 0x24;
            this.label1.Text = "메시지";
            this.label2.AutoSize = true;
            this.label2.Location = new Point(0x87, 0x1d8);
            this.label2.Name = "label2";
            this.label2.Size = new Size(0x29, 12);
            this.label2.TabIndex = 0x25;
            this.label2.Text = "아이템";
            this.CafeCheckBox.AutoSize = true;
            this.CafeCheckBox.Location = new Point(400, 0x1d8);
            this.CafeCheckBox.Name = "CafeCheckBox";
            this.CafeCheckBox.Size = new Size(50, 0x10);
            this.CafeCheckBox.TabIndex = 0x26;
            this.CafeCheckBox.Text = "Cafe";
            this.CafeCheckBox.UseVisualStyleBackColor = true;
            this.ExtendExpireTimeButton.Location = new Point(12, 0x20b);
            this.ExtendExpireTimeButton.Name = "ExtendExpireTimeButton";
            this.ExtendExpireTimeButton.Size = new Size(0x75, 0x16);
            this.ExtendExpireTimeButton.TabIndex = 0x27;
            this.ExtendExpireTimeButton.Text = "캐시템 연장";
            this.ExtendExpireTimeButton.UseVisualStyleBackColor = true;
            this.ExtendExpireTimeButton.Click += new EventHandler(this.ExtendExpireTimeButton_Click);
            int[] numArray4 = new int[4];
            numArray4[0] = 10;
            this.CashExtendMinutesUpDown1.Increment = new decimal(numArray4);
            this.CashExtendMinutesUpDown1.Location = new Point(0xe5, 0x20c);
            int[] numArray5 = new int[4];
            numArray5[0] = 0x540be400;
            numArray5[1] = 2;
            this.CashExtendMinutesUpDown1.Maximum = new decimal(numArray5);
            this.CashExtendMinutesUpDown1.Name = "CashExtendMinutesUpDown1";
            this.CashExtendMinutesUpDown1.Size = new Size(0x36, 0x15);
            this.CashExtendMinutesUpDown1.TabIndex = 40;
            this.CashExtendMinutesUpDown1.TextAlign = HorizontalAlignment.Right;
            int[] numArray6 = new int[4];
            numArray6[0] = 10;
            this.CashExtendMinutesUpDown1.Value = new decimal(numArray6);
            this.label3.AutoSize = true;
            this.label3.Location = new Point(0x88, 0x210);
            this.label3.Name = "label3";
            this.label3.Size = new Size(0x55, 12);
            this.label3.TabIndex = 0x29;
            this.label3.Text = "현재 시간 기준";
            this.label4.AutoSize = true;
            this.label4.Location = new Point(0x121, 0x210);
            this.label4.Name = "label4";
            this.label4.Size = new Size(0x2d, 12);
            this.label4.TabIndex = 0x2a;
            this.label4.Text = "분 연장";
            this.GameServerCommandButton.Location = new Point(12, 0x17f);
            this.GameServerCommandButton.Name = "GameServerCommandButton";
            this.GameServerCommandButton.Size = new Size(0x75, 0x16);
            this.GameServerCommandButton.TabIndex = 0x17;
            this.GameServerCommandButton.Text = "서버 커맨드";
            this.GameServerCommandButton.UseVisualStyleBackColor = true;
            this.GameServerCommandButton.Click += new EventHandler(this.GameServerCommandButton_Click);
            base.AutoScaleDimensions = new SizeF(7f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1ce, 580);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.CashExtendMinutesUpDown1);
            base.Controls.Add(this.ExtendExpireTimeButton);
            base.Controls.Add(this.CafeCheckBox);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.ItemFestivalMessageTextBox);
            base.Controls.Add(this.ItemFestivalAmountUpDown);
            base.Controls.Add(this.ItemFestivalClassNameTextBox);
            base.Controls.Add(this.ItemFestivalButton);
            base.Controls.Add(this.ShurinkButton_a);
            base.Controls.Add(this.ExpandButton_c);
            base.Controls.Add(this.ShurinkButton_b);
            base.Controls.Add(this.ExpandButton_b);
            base.Controls.Add(this.CopyToClipBoardButton);
            base.Controls.Add(this.ExpandButton_a);
            base.Controls.Add(this.ShurinkButton_c);
            base.Controls.Add(this.ClientCommandButton);
            base.Controls.Add(this.GameServerCommandButton);
            base.Controls.Add(this.HostCommandButton);
            base.Controls.Add(this.ConsoleCommandTextBox);
            base.Controls.Add(this.ResumeServiceSelectComboBox);
            base.Controls.Add(this.StopServiceSelectComboBox);
            base.Controls.Add(this.ResumeServiceButton);
            base.Controls.Add(this.StopServiceButton);
            base.Controls.Add(this.CharacterIDLabel);
            base.Controls.Add(this.UserIDLabel);
            base.Controls.Add(this.UserCountTextBox);
            base.Controls.Add(this.NotifyTextBox);
            base.Controls.Add(this.KickUserButton);
            base.Controls.Add(this.NotifyButton);
            base.Controls.Add(this.RefreshUserCountButton);
            base.Controls.Add(this.ShutDownTimeListBox);
            base.Controls.Add(this.KickCharacterIDTextBox);
            base.Controls.Add(this.KickUserIDTextBox);
            base.Controls.Add(this.ShutDownBottuon);
            base.MaximizeBox = false;
            base.Name = "AdminClientForm";
            this.Text = "Heroes Server Admin Client";
            base.Load += new EventHandler(this.AdminClientForm_Load);
            base.Resize += new EventHandler(this.AdminClientForm_Resize);
            this.ItemFestivalAmountUpDown.EndInit();
            this.CashExtendMinutesUpDown1.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void ItemFestivalButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            string text = this.ItemFestivalClassNameTextBox.Text;
            if (text == "")
            {
                text = "gold";
            }
            int num = (int) this.ItemFestivalAmountUpDown.Value;
            if (num <= 0)
            {
                num = 1;
            }
            string str2 = this.ItemFestivalMessageTextBox.Text;
            if (str2 == "")
            {
                builder.AppendLine("메시지가 없습니다!");
                MessageBox.Show(builder.ToString(), "아이템 지급 이벤트 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.Enabled = true;
            }
            else
            {
                builder.AppendLine(str2);
                builder.AppendLine("---");
                builder.AppendLine(string.Format("{0} {1:n}개를 유저 전체에게 지급합니다.", text, num));
                builder.AppendLine("명령을 실행할까요?");
                if (MessageBox.Show(builder.ToString(), "아이템 지급 이벤트", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    this.adminNode.RequestItemFestival(text, num, str2, this.CafeCheckBox.Checked);
                }
                base.Enabled = true;
            }
        }

        private void KickCharacterIDTextBox_Enter(object sender, EventArgs e)
        {
            this.KickCharacterIDTextBox.SelectAll();
        }

        private void KickCharacterIDTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void KickUserButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            if (((this.KickUserIDTextBox.Text != "") && (this.KickCharacterIDTextBox.Text != "")) || ((this.KickCharacterIDTextBox.Text == "") && (this.KickUserIDTextBox.Text == "")))
            {
                builder.AppendLine("ID 인식에 실패했습니다.");
                MessageBox.Show(builder.ToString(), "Kick 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.Enabled = true;
            }
            else
            {
                if (this.KickCharacterIDTextBox.Text != "")
                {
                    builder.AppendLine(this.KickCharacterIDTextBox.Text);
                }
                else
                {
                    builder.AppendLine(this.KickUserIDTextBox.Text);
                }
                builder.AppendLine();
                builder.AppendLine("---");
                builder.AppendLine();
                builder.AppendLine("이 유저를 Kick합니다!");
                if (MessageBox.Show(builder.ToString(), "공지", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    this.adminNode.RequestKick(this.KickUserIDTextBox.Text, this.KickCharacterIDTextBox.Text);
                }
                base.Enabled = true;
            }
        }

        private void KickUserIDTextBox_Enter(object sender, EventArgs e)
        {
            this.KickUserIDTextBox.SelectAll();
        }

        private void KickUserIDTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void NotifyButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(this.NotifyTextBox.Text);
            builder.AppendLine();
            builder.AppendLine("---");
            builder.AppendLine();
            builder.AppendLine("중간의 빈 줄은 자동적으로 무시됩니다.");
            builder.AppendLine("위 내용을 공지할까요?");
            if (MessageBox.Show(builder.ToString(), "공지", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                this.adminNode.RequestNotify(this.NotifyTextBox.Text);
            }
            base.Enabled = true;
        }

        private void NotifyTextBox_Enter(object sender, EventArgs e)
        {
            this.NotifyTextBox.SelectAll();
        }

        private void OnConnected(object sender, EventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Admin 서버에 접속하였습니다.");
            MessageBox.Show(builder.ToString(), "접속 성공", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Admin 서버의 접속이 끊어졌습니다");
            builder.AppendLine();
            builder.AppendLine("프로그램을 종료합니다.");
            MessageBox.Show(builder.ToString(), "접속 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            base.Close();
        }

        private void OnFailed(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Admin 서버에 접속하지 못하였습니다.");
            builder.AppendLine();
            builder.AppendLine("프로그램을 종료합니다.");
            MessageBox.Show(builder.ToString(), "접속 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            base.Close();
        }

        private void RefreshUserCountButton_Click(object sender, EventArgs e)
        {
            this.adminNode.RequestUserCount();
        }

        private void ResumeServiceButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            if (this.ResumeServiceSelectComboBox.SelectedItem == null)
            {
                builder.AppendLine("정지 서비스가 선택되지 않았습니다!");
                MessageBox.Show(builder.ToString(), "서비스 재개 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.Enabled = true;
            }
            else
            {
                char[] separator = new char[] { ' ' };
                string[] strArray = ((string) this.ResumeServiceSelectComboBox.SelectedItem).Split(separator);
                builder.AppendLine(string.Format("서비스를 재개합니다 : {0}.", strArray[0]));
                builder.AppendLine();
                builder.AppendLine("계속할까요?");
                if (MessageBox.Show(builder.ToString(), "서비스 재개", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    this.adminNode.RequestStopService(strArray[0], false);
                }
                base.Enabled = true;
            }
        }

        private void ShurinkButton_b_Click(object sender, EventArgs e)
        {
            base.Height = this.formHeight = 190;
            this.NotifyButton.Visible = this.NotifyTextBox.Visible = false;
            this.ClientCommandButton.Visible = this.ConsoleCommandTextBox.Visible = false;
            this.ShurinkButton_a.Visible = this.ExpandButton_a.Visible = true;
            this.ShurinkButton_b.Visible = this.ExpandButton_b.Visible = false;
            this.ShurinkButton_c.Visible = this.ExpandButton_c.Visible = false;
        }

        private void ShurinkButton_c_Click(object sender, EventArgs e)
        {
            base.Height = this.formHeight = 370;
            this.NotifyButton.Visible = this.NotifyTextBox.Visible = true;
            this.ClientCommandButton.Visible = this.ConsoleCommandTextBox.Visible = false;
            this.ShurinkButton_a.Visible = this.ExpandButton_a.Visible = false;
            this.ShurinkButton_b.Visible = this.ExpandButton_b.Visible = true;
            this.ShurinkButton_c.Visible = this.ExpandButton_c.Visible = false;
        }

        private void ShutDownBottuon_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            if (this.ShutDownTimeListBox.SelectedItem == null)
            {
                builder.AppendLine("서버 종료 시간이 선택되지 않았습니다!");
                MessageBox.Show(builder.ToString(), "서버 종료 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.Enabled = true;
            }
            else
            {
                char[] separator = new char[] { ' ' };
                string[] strArray = ((string) this.ShutDownTimeListBox.SelectedItem).Split(separator);
                builder.AppendLine(string.Format("{0}초 후에 서버를 종료합니다.", strArray[0]));
                builder.AppendLine();
                builder.AppendLine("계속할까요?");
                if (MessageBox.Show(builder.ToString(), "서버 종료", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    this.adminNode.RequestShutDown(int.Parse(strArray[0]) + 1, "");
                }
                base.Enabled = true;
            }
        }

        private void StopServiceButton_Click(object sender, EventArgs e)
        {
            base.Enabled = false;
            StringBuilder builder = new StringBuilder();
            if (this.StopServiceSelectComboBox.SelectedItem == null)
            {
                builder.AppendLine("정지 서비스가 선택되지 않았습니다!");
                MessageBox.Show(builder.ToString(), "긴급 정지 실패!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                base.Enabled = true;
            }
            else
            {
                char[] separator = new char[] { ' ' };
                string[] strArray = ((string) this.StopServiceSelectComboBox.SelectedItem).Split(separator);
                builder.AppendLine(string.Format("서비스를 긴급 정지합니다 : {0}.", strArray[0]));
                builder.AppendLine();
                builder.AppendLine("계속할까요?");
                if (MessageBox.Show(builder.ToString(), "긴급 정지", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    this.adminNode.RequestStopService(strArray[0], true);
                }
                base.Enabled = true;
            }
        }

        public void UpdateStoppedServices(List<string> target)
        {
            char[] separator = new char[] { ' ' };
            this.StopServiceSelectComboBox.SelectedItem = null;
            this.ResumeServiceSelectComboBox.SelectedItem = null;
            List<object> list = new List<object>();
            foreach (object obj2 in this.StopServiceSelectComboBox.Items)
            {
                if (obj2 is string)
                {
                    string[] strArray = (obj2 as string).Split(separator);
                    if (target.Contains(strArray[0]))
                    {
                        list.Add(obj2);
                    }
                }
            }
            foreach (object obj3 in list)
            {
                this.ResumeServiceSelectComboBox.Items.Add(obj3);
                this.StopServiceSelectComboBox.Items.Remove(obj3);
            }
            list.Clear();
            foreach (object obj4 in this.ResumeServiceSelectComboBox.Items)
            {
                if (obj4 is string)
                {
                    string[] strArray2 = (obj4 as string).Split(separator);
                    if (!target.Contains(strArray2[0]))
                    {
                        list.Add(obj4);
                    }
                }
            }
            foreach (object obj5 in list)
            {
                this.StopServiceSelectComboBox.Items.Add(obj5);
                this.ResumeServiceSelectComboBox.Items.Remove(obj5);
            }
            if (this.ResumeServiceSelectComboBox.Items.Count > 0)
            {
                this.ResumeServiceSelectComboBox.BackColor = Color.Red;
                this.StopServiceSelectComboBox.BackColor = Color.Red;
            }
            else
            {
                this.ResumeServiceSelectComboBox.BackColor = Color.White;
                this.StopServiceSelectComboBox.BackColor = Color.White;
            }
        }

        public void UpdateUserCountText(int count, int total, int waiting, Dictionary<string, int> states)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("[{0}] {1:N0} Char / {2:N0} User ({3:N0} Wait) ", new object[] { DateTime.Now.ToString("yyMMdd HH:mm:ss"), count, total, waiting });
            foreach (KeyValuePair<string, int> pair in states)
            {
                builder.AppendFormat("[{0}={1}]", pair.Key, pair.Value);
            }
            builder.AppendLine();
            this.UserCountTextBox.AppendText(builder.ToString());
            if ((this.LatestUserCount > 0x3e8) && (((this.LatestUserCount - total) * 20) > this.LatestUserCount))
            {
                MessageBox.Show(string.Format("동시 접속자 숫자가 {0:0.0}% 감소했습니다.", ((this.LatestUserCount - total) * 100.0) / ((double) this.LatestUserCount)), "동시 접속자 수 감소!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            this.LatestUserCount = total;
        }

        public MessageHandlerFactory MF
        {
            get
            {
                return this.mf;
            }
        }

        public JobProcessor Thread
        {
            get
            {
                return this.thread;
            }
        }
    }
}

