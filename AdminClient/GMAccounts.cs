﻿namespace AdminClient
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Table(Name="dbo.GMAccounts")]
    public class GMAccounts : INotifyPropertyChanging, INotifyPropertyChanged
    {
        private string _ID;
        private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(string.Empty);

        public event PropertyChangedEventHandler PropertyChanged;

        public event PropertyChangingEventHandler PropertyChanging;

        protected virtual void SendPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected virtual void SendPropertyChanging()
        {
            if (this.PropertyChanging != null)
            {
                this.PropertyChanging(this, emptyChangingEventArgs);
            }
        }

        [Column(Storage="_ID", DbType="NVarChar(20) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
        public string ID
        {
            get
            {
                return this._ID;
            }
            set
            {
                if (this._ID != value)
                {
                    this.SendPropertyChanging();
                    this._ID = value;
                    this.SendPropertyChanged("ID");
                }
            }
        }
    }
}

