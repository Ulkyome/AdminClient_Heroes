﻿namespace AdminClient.Properties
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0"), CompilerGenerated]
    internal sealed class Settings : ApplicationSettingsBase
    {
        private static Settings defaultInstance = ((Settings) SettingsBase.Synchronized(new Settings()));

        [DefaultSettingValue("211.39.128.79"), DebuggerNonUserCode, ApplicationScopedSetting]
        public string ConnectIP
        {
            get
            {
                return (string) this["ConnectIP"];
            }
        }

        [ApplicationScopedSetting, DebuggerNonUserCode, DefaultSettingValue("27011")]
        public ushort ConnectPort
        {
            get
            {
                return (ushort) this["ConnectPort"];
            }
        }

        public static Settings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue("Data Source=211.39.128.122;Initial Catalog=heroes;User ID=devcat-temp;Password=neb-x7wrENu_A_@drer@phaPrADr#hafru?aphUcUswapuZAnaTe$u5ra*rechap"), ApplicationScopedSetting, SpecialSetting(SpecialSetting.ConnectionString)]
        public string heroesConnectionString
        {
            get
            {
                return (string) this["heroesConnectionString"];
            }
        }
    }
}

